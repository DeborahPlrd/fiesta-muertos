<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLinesTableConstraint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('slate_lines', function (Blueprint $table) {
            $table->foreign('slate_id')->references('id')->on('slates');
            $table->foreign('player_id')->references('id')->on('players');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('slate_lines', function (Blueprint $table) {
            $table->dropForeign('slate_id');
            $table->dropForeign('player_id');
        });
    }
}
