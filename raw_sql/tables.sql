CREATE TABLE Players
(
    Id Int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    Nickname Varchar (50) NOT NULL,
    Mail Varchar (50) NOT NULL,
    Password Varchar (50) NOT NULL,
    Token Varchar (250) NOT NULL,
    Score Int NOT NULL,
    Play_order Int NOT NULL,
    Bone Int,
    Id_Skulls Int NOT NULL,
    Id_Characters Int NOT NULL,
    CONSTRAINT Players_Skulls_FK FOREIGN KEY (Id_Skulls) REFERENCES Skulls(Id),
    CONSTRAINT Players_Characters_FK FOREIGN KEY (Id_Characters) REFERENCES Characters(Id)
);

CREATE TABLE Games
(
    Id Int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    Game_result Int
);

CREATE TABLE Skulls
(
    Id Int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    Turn Int NOT NULL,
    Cases BOOLEAN NOT NULL,
    Locked BOOLEAN NOT NULL,
    Word VARCHAR (50) NOT NULL
);

CREATE TABLE Characters
(
    Id Int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    Name VARCHAR (50) NOT NULL,
    Taken BOOLEAN NOT NULL
);

CREATE TABLE Slates
(
    Id Int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    Impostor VARCHAR (50)
);

CREATE TABLE Slate_lines
(
    Id Int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    Number Int NOT NULL,
    Answer VARCHAR (50),
    Id_Players Int NOT NULL,
    Id_Slates Int NOT NULL,
    CONSTRAINT Slate_lines_Players_FK FOREIGN KEY (Id_Players) REFERENCES Players(Id),
    CONSTRAINT Slate_lines_Slates_FK FOREIGN KEY (Id_Slates) REFERENCES Slates(Id)
);

CREATE TABLE Difficulties
(
    Id Int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    Category VARCHAR (250),
    Description VARCHAR (250)
);

CREATE TABLE Rules
(
    Id Int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    Description VARCHAR (250),
    Name VARCHAR (50),
    Default_rule VARCHAR (250) NOT NULL,
    Set_default_rule BOOLEAN NOT NULL
);
