<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    use HasFactory;

    protected $fillable = [
        "nickname", "mail", "password", "skull_id", "character_id"
    ];

    public function skull() {
        return $this->belongsTo(Skull::class, "skull_id");
    }

    public function character() {
        return $this->belongsTo(Character::class, "character_id");
    }
}
