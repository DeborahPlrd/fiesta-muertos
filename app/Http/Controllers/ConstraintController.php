<?php

namespace App\Http\Controllers;

use App\Models\Constraint;
use Illuminate\Http\Request;

class ConstraintController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Constraint  $constraint
     * @return \Illuminate\Http\Response
     */
    public function show(Constraint $constraint)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Constraint  $constraint
     * @return \Illuminate\Http\Response
     */
    public function edit(Constraint $constraint)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Constraint  $constraint
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Constraint $constraint)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Constraint  $constraint
     * @return \Illuminate\Http\Response
     */
    public function destroy(Constraint $constraint)
    {
        //
    }
}
