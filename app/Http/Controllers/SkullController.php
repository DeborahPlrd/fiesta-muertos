<?php

namespace App\Http\Controllers;

use App\Models\Skull;
use Illuminate\Http\Request;

class SkullController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Skull  $skull
     * @return \Illuminate\Http\Response
     */
    public function show(Skull $skull)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Skull  $skull
     * @return \Illuminate\Http\Response
     */
    public function edit(Skull $skull)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Skull  $skull
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Skull $skull)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Skull  $skull
     * @return \Illuminate\Http\Response
     */
    public function destroy(Skull $skull)
    {
        //
    }
}
