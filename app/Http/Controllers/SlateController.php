<?php

namespace App\Http\Controllers;

use App\Models\Slate;
use Illuminate\Http\Request;

class SlateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Slate  $slate
     * @return \Illuminate\Http\Response
     */
    public function show(Slate $slate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Slate  $slate
     * @return \Illuminate\Http\Response
     */
    public function edit(Slate $slate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Slate  $slate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slate $slate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Slate  $slate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slate $slate)
    {
        //
    }
}
