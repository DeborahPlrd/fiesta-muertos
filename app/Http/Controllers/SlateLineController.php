<?php

namespace App\Http\Controllers;

use App\Models\SlateLine;
use Illuminate\Http\Request;

class SlateLineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SlateLine  $slateLine
     * @return \Illuminate\Http\Response
     */
    public function show(SlateLine $slateLine)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SlateLine  $slateLine
     * @return \Illuminate\Http\Response
     */
    public function edit(SlateLine $slateLine)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SlateLine  $slateLine
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SlateLine $slateLine)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SlateLine  $slateLine
     * @return \Illuminate\Http\Response
     */
    public function destroy(SlateLine $slateLine)
    {
        //
    }
}
