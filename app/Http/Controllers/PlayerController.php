<?php

namespace App\Http\Controllers;

use App\Models\Character;
use App\Models\Player;
use App\Models\Skull;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

class PlayerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|View
     */
    public function index()
    {
        $players = Player::all();
        return view("players/index", [
            'players' => $players
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        $character = Character::all();
        $skull = Skull::all();

        return view("players.create", compact($character, $skull));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {

        $player = new Player();
        $player->nickname = $request->get("nickname");
        $player->mail = $request->get("mail");
        $player->password = "";
        $player->token = "";
        $player->score = $request->get("score") != null ? $request->get("score") : 0;
        $player->play_order = $request->get("play_order") != null ? $request->get("play_order") : 0;
        $player->bones = $request->get("bones") != null ? $request->get("bones") : 0;
        $player->skull_id = $request->get("skull_id") != null ? $request->get("skull_id") : "Non-renseigné";
        $player->character_id = $request->get("character_id") != null ? $request->get("character_id") : "Non-renseigné";

        $player->save();

        return redirect()->route("players.index");
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|View
     */
    public function show($id)
    {
        $player = Player::find(1);

        return view("players/show", [
            'player' => $player
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return View
     */
    public function edit($id)
    {
        $player = Player::find($id);
        $character = Character::all();
        $skull = Skull::all();

        return view("players.edit", compact($player, $character, $skull));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $player = Player::find($id);
        $player->nickname = $request->get("nickname");
        $player->mail = $request->get("mail");
        $player->score = $request->get("score") != null ? $request->get("score") : 0;
        $player->play_order = $request->get("play_order") != null ? $request->get("play_order") : 0;
        $player->bones = $request->get("bones") != null ? $request->get("bones") : 0;
        $player->skull_id = $request->get("skull_id") != null ? $request->get("skull_id") : "Non-renseigné";
        $player->character_id = $request->get("character_id") != null ? $request->get("character_id") : "Non-renseigné";

        $player->save();

        return redirect()->route("players.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function destroy(Request $request)
    {
        $player = Player::find($request->get("id"));
        $player->delete();

        return redirect()->route("players.index");
    }
}
